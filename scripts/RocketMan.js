import { options } from './options.js';
import Menu from './states/Menu.js';
import Game from './states/Game.js';


export default class RocketMan {
	constructor() {
		// this._options = options;
		this.stage = new createjs.Stage('canvas-container');
		this.loader = new createjs.LoadQueue(false);
		this.loadAssets();
		this.Menu;
		this.Game;
	}
	loadAssets() {
		this.loader.on("complete", this.handleLoadComplete, this);
		this.loader.loadManifest(options.manifest, true, "./assets/img/");
	}

	startGame() {
		this.stage.removeAllChildren();
		this.stage.update();
		this.Game = new Game(this.stage, options);
	}	

	handleLoadComplete(event) {
		this.Menu = new Menu(this.stage, options, () => {
			this.startGame();
		});
	}
}




