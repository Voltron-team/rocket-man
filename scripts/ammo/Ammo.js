export default class Ammo {
	constructor(options) {
		this.id = options.id;
		this.speed = options.speed;
		this.spriteSheet = options.spriteSheet;
		this.explosion = options.explosionAnimation;
		this.sprite;
	}

	fire(options, stage) {
		let ammoSpriteSheet = new createjs.SpriteSheet(this.spriteSheet);
		this.sprite = new createjs.Sprite(ammoSpriteSheet);
		let ammoSprite = this.sprite;
		ammoSprite.x = options.fromX;
		ammoSprite.y = options.fromY;
		ammoSprite.rotation = options.rotation;
		ammoSprite.regX = this.spriteSheet.frames.width / 2;
		ammoSprite.regY = this.spriteSheet.frames.height / 2;
		ammoSprite.framerate = 60;
		
		stage.addChild(ammoSprite);

		let explosionSpriteSheet = new createjs.SpriteSheet(this.explosion.spriteSheet);
		let explosionSprite = new createjs.Sprite(explosionSpriteSheet);
		explosionSprite.regX = this.explosion.spriteSheet.frames.width / 2;
		explosionSprite.regY = this.explosion.spriteSheet.frames.height / 2;
		explosionSprite.framerate = 60;

		let distance = Math.hypot(options.toX - ammoSprite.x, options.toY - ammoSprite.y);
		let duration = distance / this.speed;
		ammoSprite.play("fly");
		TweenMax.to(ammoSprite, duration, {
			x: options.toX,
			y: options.toY,
			ease: Linear.easeNone,
			onComplete() {
				explosionSprite.x = ammoSprite.x;
				explosionSprite.y = ammoSprite.y;
				stage.removeChild(ammoSprite);
				ammoSprite.gotoAndStop(0);
				stage.addChild(explosionSprite);
				explosionSprite.gotoAndPlay('explode');
			}
		});

		explosionSprite.on("animationend", function(target, type, name, next) {
			stage.removeChild(target.currentTarget);
		});
	}
}