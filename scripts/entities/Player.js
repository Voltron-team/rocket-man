import Entity from './Entity.js';
import Ammo from '../ammo/Ammo.js';

export default class Player extends Entity {
	constructor(options, position, ammoOptions) {
		super(options);
		this._initialPosition = position;
		this._options = options;

		this.container = new createjs.Container();
		this.container.x = this._initialPosition.x;
		this.container.y = this._initialPosition.y;

		let playerSpriteSheet = new createjs.SpriteSheet(options.spriteSheet);
		this.sprite = new createjs.Sprite(playerSpriteSheet);
		this.sprite.framerate = 60;
		this.sprite.regX = 14;
		this.sprite.name = 'playerSprite';
		this.container.addChild(this.sprite);

		this.laser = initLaser(this.sprite, this._options.laser);
		this.container.addChild(this.laser);

		this.ammo = new Ammo(ammoOptions);
		
	}

	tryRotate(target) {
		let rads = Math.atan2(target.y - this.container.y, target.x - this.container.x);
		let angle = rads * (180 / Math.PI);
		this.container.rotation = angle + 90;
	}

	tryMove(target, worldBounds) {
		let container = this.container;
		let sprite = this.sprite;
		let distance = Math.hypot(target.x - container.x, target.y - container.y);
		let duration = distance / this.speed;
		
		target.x = target.x < container.regX ? container.regX : target.x;
		target.x = target.x > worldBounds.maxX - container.regX ? worldBounds.maxX - container.regX : target.x;

		target.y = target.y < container.regY ? container.regY : target.y;
		target.y = target.y > worldBounds.maxY - container.regY ? worldBounds.maxY - container.regY : target.y;	
		sprite.play("walk");	
		TweenMax.to(container, duration, {
			x: target.x,
			y: target.y,
			ease: Linear.easeNone,
			onComplete() {
				sprite.gotoAndStop(0);
			}
		});

	}

	tryFire(target, stage) {
		if (this.ammo && this.ammo.spriteSheet) {
			let options = {
				fromX: this.container.x,
				fromY: this.container.y,
				toX: target.x,
				toY: target.y,
				rotation: this.container.rotation
			};
			this.ammo.fire(options, stage);
		}
 	}
}

function initLaser(playerSprite, laserOptions) {
	let laserDimensions = {
		beginX: 0,
		beginY: -playerSprite.regY,
		endX: 0,
		endY: -playerSprite.regY - laserOptions.len
	};
	let laser = new createjs.Shape();
	
	laser.graphics.setStrokeStyle(laserOptions.thickness)
		.beginLinearGradientStroke(laserOptions.colors, [0.4, 1], laserDimensions.beginX, laserDimensions.beginY, laserDimensions.endX, laserDimensions.endY)
		.moveTo(laserDimensions.beginX, laserDimensions.beginY)
		.lineTo(laserDimensions.endX, laserDimensions.endY);
	laser.name = 'playerLaser';

	return laser;
}