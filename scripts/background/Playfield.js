export default class Playfield {
	constructor(options, position) {
		this.background = new createjs.Shape();
		this.background.graphics.beginFill(options.background.color);
		this.background.graphics.drawRect(position.x[0], position.y[0], position.x[1], position.y[1]);
		this.background.graphics.endFill();
		this.background.name = options.id;
	}
}