import Playfield from '../background/playfield.js';


export default class Menu {
	constructor(stage, options, launchGame) {
		this.stage = stage;
		this.stageWidth = this.stage.canvas.width;
		this.stageHeight = this.stage.canvas.height;
		this.Playfield = new Playfield(options.playfieldOptions, {
			x: [0, this.stage.canvas.width],
			y: [0, this.stage.canvas.height]
		});
		this.stage.addChild(this.Playfield.background);

		this.controlsContainer = new createjs.Container();
		this.controlsContainer.x = 80;
		this.controlsContainer.y = 205;
		this.controlsContainer.setBounds(this.controlsContainer.x, this.controlsContainer.y, this.stageWidth - 2 * this.controlsContainer.x, this.stageHeight - 2 * this.controlsContainer.y);

		let outerControlsHolder = createRoundRect({
			isRound: true,
			color: '#333638',
			x: 0,
			y: 0,
			w: this.controlsContainer.getBounds().width,
			h: this.controlsContainer.getBounds().height,
			r: 15
		});
		this.controlsContainer.addChild(outerControlsHolder);


		let innerControlsHolder = createRoundRect({
			isRound: true,
			color: '#3E4144',
			x: 10,
			y: 10,
			w: this.controlsContainer.getBounds().width - 2 * 10,
			h: this.controlsContainer.getBounds().height - 2 * 10,
			r: 8
		});
		this.controlsContainer.addChild(innerControlsHolder);


	    let startGameBtn = createRoundRect({
			isRound: true,
			color: '#8BC558',
			x: 245,
			y: 240,
			w: this.controlsContainer.getBounds().width - 2 * 245,
			h: this.controlsContainer.getBounds().height - 2 * 240,
			r: 5
		});
	    this.controlsContainer.addChild(startGameBtn);

	    let startGameTxt = new createjs.Text('Start Game', '30px Times New Roman', '#FFF');
	    startGameTxt.x = this.controlsContainer.getBounds().width / 2;
	    startGameTxt.y = this.controlsContainer.getBounds().height / 2;
	    startGameTxt.textAlign = "center";
	    startGameTxt.textBaseline = "middle";
	    this.controlsContainer.addChild(startGameTxt);


	    startGameBtn.on('click',() => {
			TweenMax.to(this.controlsContainer, 0.5, {
				y: -this.controlsContainer.getBounds().height,
				ease: Back.easeIn,
				onComplete: () => {
					createjs.Ticker.removeAllEventListeners();
					launchGame();
				}
			});
	    });

	    this.stage.addChild(this.controlsContainer);

	    // Setup the ticker
		// createjs.Ticker.setFPS(60);
		createjs.Ticker.on("tick", this.handleTick, this);
	    this.stage.update();
	}

	handleTick() {
		this.stage.update();
	}
}

function createRoundRect(options) {
	let shape = new createjs.Shape();

	shape.graphics.beginFill(options.color);
	if (!options.isRound) {
		shape.graphics.drawRect(options.x, options.y, options.w, options.h);
	} else {
		shape.graphics.drawRoundRect(options.x, options.y, options.w, options.h, options.r);
	}
	shape.graphics.endFill();
    shape.setBounds(options.x, options.y, options.w, options.h);
    
    return shape;
}