import Player from '../entities/player.js';
import Playfield from '../background/playfield.js';


export default class Game {
	constructor(stage, options) {
		this.playerOptions = options.playerOptions;
		this.ammoOptions = options.ammoOptions;
		this.playfieldOptions = options.playfieldOptions;
		this.miscAnimations = options.miscAnimations;
		
		// Set up stage
		this.stage = stage;
		this.stageWidth = this.stage.canvas.width;
		this.stageHeight = this.stage.canvas.height;

		// Register mouse
		// this.stage.mouseMoveOutside = true;
		this.stage.on("stagemousemove", evt => {
		    this.Player.tryRotate({x: evt.stageX, y: evt.stageY});
		});

		this.stage.on("click", evt => {
			if (evt.nativeEvent.button === 0) { // Left click
				if (this.Player)
					this.Player.tryFire({x: evt.stageX, y: evt.stageY}, this.stage);
			}

			if (evt.nativeEvent.button === 2) { // Right click
				if (this.Player) {
					ripple({x: evt.stageX, y: evt.stageY}, this.stage);
					this.Player.tryMove({x: evt.stageX, y: evt.stageY}, {maxX: this.stageWidth, maxY: this.stageHeight});
				}
			}
		});

		// Set up background
		this.Playfield = new Playfield(this.playfieldOptions, {
			x: [0, this.stageWidth],
			y: [0, this.stageHeight]
		});
		this.stage.addChild(this.Playfield.background);

		// Set up player
		let playerPosition = {
			// x: this.stageWidth / 2,
			// y: this.stageHeight - this.playerOptions.spriteSheet.frames.height
			x: 200,
			y: 200
		};
		this.Player = new Player(this.playerOptions, playerPosition, this.ammoOptions[this.playerOptions.ammo]);
		this.stage.addChild(this.Player.container);

		this.stage.update();

		// Set up the ticker
		createjs.Ticker.on("tick", this.handleTick, this);
	}

	handleTick() {
		this.stage.update();
	}
}

function ripple(target, stage) {
	let pointerShape = new createjs.Shape();
	pointerShape.graphics
		.setStrokeStyle(5)
		.beginStroke('#FF0000')
		.drawCircle(0, 0, 20);
	pointerShape.x = target.x;
	pointerShape.y = target.y;
	pointerShape.alpha = 0;
	stage.addChild(pointerShape);

	TweenMax.from(pointerShape, 0.7, {
		x: pointerShape.x,
		y: pointerShape.y,
		scale: 0,
		alpha: 1,
		ease: Power1.easeOut,
		onComplete: () => stage.removeChild(pointerShape)
	});
}