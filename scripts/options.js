// Set up defaults
let options = {};
options.playfieldOptions = {
	background: {
		id: 'background',
		color: '#252729'
	}
};
options.playerOptions = {
	id: 'player',
	speed: 250,
	ammo: 'rocket',
	spriteSheet: {
		images: ['./assets/img/sniper.png'],
		frames: {
			height: 63,
			count: 8, 
			width: 52,
			spacing: 0.875
		},
		animations: {
			walk: [1, 7]
		},
	},
	laser: {
		len: 500,
		colors: ['#FF0000', options.playfieldOptions.background.color],
		thickness: 0.5
	}
};
options.miscAnimations = {
	explosion: {
		id: 'explosion',
		spriteSheet: {
			images: ['./assets/img/explosion.png'],
			frames: {
				height: 128,
				count: 40,
				width: 128,
				// spacing: 3
			},
			animations: {
				explode: [0, 39, false]
			}
		}
	}
};
options.ammoOptions = {
	rocket: {
		id: 'rocket',
		speed: 700,
		spriteSheet: {
			images: [ './assets/img/rocket.png' ],
			frames: {
				height: 49,
				count: 4,
				width: 26
			},
			animations: {
				fly:[0, 3]
			}
		},
		explosionAnimation: options.miscAnimations.explosion
		
	}
};

options.manifest = [
	// {src: this.playfieldOptions.background.image, id: this.playfieldOptions.background.id},
	{src: options.playerOptions.spriteSheet, id: options.playerOptions.id},
	{src: options.ammoOptions.rocket.spriteSheet, id: options.ammoOptions.rocket.id},
	{src: options.miscAnimations.explosion.spriteSheet, id: options.miscAnimations.explosion.id}
];

export { options };