var express = require('express');

var app = express();

app.use(express.static(__dirname + '/build'));

app.listen(8888);
console.log('Listen at port: 8888');
