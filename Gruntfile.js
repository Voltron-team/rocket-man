module.exports = function(grunt) {
	// grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-watch');
	// grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-express-server');
	grunt.loadNpmTasks('grunt-browserify');
	// require('load-grunt-tasks')(grunt);
	
	grunt.registerTask('default', ['clean', 'browserify', 'copy']);
	grunt.registerTask('serve', ['default', 'express', 'watch']);

	grunt.initConfig({
		clean: ['tmp', 'build'],
		copy: {
			assets: {
				expand: true,
				src: ['assets/**'],
				dest: 'build/',
			},
			index: {
				src: ['index.html'],
				dest: 'build/'
			}
		},
		watch: {
			options: {  livereload: true,  },
			express: {
				files:  [ 'scripts/**/*.js', 'assets/**/*.*', 'index.html', 'server.js' ],
				tasks:  [ 'express:server' ],
				options: {
					livereload: 1234,
				    spawn: false
			    }
			},
			scripts: {
				files: ['scripts/**/*.js', 'assets/**/*.*', 'index.html'],
				tasks: ['default']
			}
		},
		express: {
			server: {
				options: {
					script: './server.js'
				}
			}
		},
		browserify: {
			dist: {
	            options: {
	               transform: [
	                  ["babelify", {
	                     minified: true,
	                     presets: ['es2015']
	                  }]
	               ]
	            },
	            files: {
	               "./build/scripts/main.js": ["./scripts/main.js"]
	            }
			}
		}
	});
};